﻿#include <stdio.h>
#include <easyx.h>

//void outtextxy(int x, int y, LPCTSTR str)



int main()
{
	initgraph(800, 600);  //创建一个800像素高，600像素宽的窗体
	setbkcolor(WHITE);    //设置背景颜色为白色
	cleardevice();        //用设置的背景的颜色来填充每一个窗体

	setbkcolor(RGB(164, 225, 202)); //设置背景颜色为绿色(字体背景)
	settextcolor(BLACK);       //设置字体的颜色，这里设置成黑色
	setlinecolor(BLACK);       //设置下划线的颜色
	setlinestyle(PS_DASH, 5);  //描边是样式和宽度
	setbkmode(TRANSPARENT);   //将字符背景颜色进行更改，系统默认将字符的背景颜色为设置的颜色

	//settextstyle(50, 0, "微软雅黑");
	//将字体设置大小和字体样式
	settextstyle(50, 0, "微软雅黑");

	LOGFONT fontStyle;    //定义了一个LOGFONT类型的变量，为了适应settextstyle
	gettextstyle(&fontStyle);

	fontStyle.lfItalic = true;    //判断字体是否是斜体
	fontStyle.lfUnderline = true; //判断字体是否有下划线
	fontStyle.lfQuality = ANTIALIASED_QUALITY;//判断字体质量
	//将字符特点设置成为上述类型
	settextstyle(&fontStyle);

	RECT rect;   //设置一个RECT类型的结构体
	rect.left = 200; 
	rect.top = 0;
	rect.right = 600;
	rect.bottom = 300;
	//利用restangle函数建立一个要在某矩形框内写的范围
	rectangle(rect.left, rect.top, rect.right, rect.bottom);
    //创建你要写入字符的首地址指针
	const char* pStr = "你好，世界。你好，世界。你好，世界。你好，世界。";
	//可以根据自己需要进行或运算，将类型进行组合
	//drawtext(pStr, &rect, DT_CENTER);
	//drawtext(pStr, &rect, DT_LEFT);
	//drawtext(pStr, &rect, DT_RIGHT);
	//drawtext(pStr, &rect, DT_TOP);
	//drawtext(pStr, &rect, DT_TOP|DT_SINGLELINE); 
	//drawtext(pStr, &rect, DT_SINGLELINE);
	//drawtext(pStr, &rect, DT_SINGLELINE|DT_WORD_ELLIPSIS);//截取未能容下的文字，并写成省略号
	//drawtext(pStr, &rect, DT_SINGLELINE|DT_BOTTOM);  //DT_BOTTOM和DT_SINGLELINE一起使用才是有效的，将字符放在底部
	//drawtext(pStr, &rect, DT_VCENTER|DT_SINGLELINE); //DT_VCENTER和DT_SINGLELINE一起使用才是有效的，将字符居中
	drawtext(pStr, &rect, DT_WORDBREAK);  //在字符显示不下时自动换行，遇到回车也会换行

	//const char* pText = "Helloworld你好，世界。";  //这个要放在下面
	//outtextxy(0, 0, pText);
	
	
	getchar();
	closegraph();
	return 0;
}