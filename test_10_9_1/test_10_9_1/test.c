#define _CRT_SECURE_NO_WARNINGS
#include "contect.h"

void menu()
{
	printf("*****************************\n");
	printf("**** 1. ADD      2. DEC  ****\n");
	printf("**** 3. SEC      4. MFY  ****\n");
	printf("**** 5. SHOW     6. SORT ****\n");
	printf("**** 0. EXIT             ****\n");
	printf("*****************************\n");
}

enum chorce {
	EXIT,  //退出通讯录
	ADD,   //增加联系人的信息
	DEC,   //删除联系人的信息
	SEC,   //查找联系人的信息
	MFY,   //修改联系人的信息
	SHOW,  //打印所有联系人的信息
	SORT   //排序联系人的信息
};

int main()
{
	int input = 0;
	//创建结构体对象
	contect con;
	//初始化通讯录
	Initcontect(&con);

	Readcontect(&con);
	do
	{
		menu();
		printf("请输入你的选择:>");
		scanf("%d", &input);
		switch (input)
		{
		case ADD:
			Addcontect(&con);
			break;
		case DEC:
			Deccontect(&con);
			break;
		case SEC:
			Seccontect(&con);
			break;
		case MFY:
			Meycontect(&con);
			break;
		case SHOW:
			Showcontect(&con);
			break;
		case SORT:
			Sortbyname(&con);
			break;
		case EXIT:
			Filecontect(&con);
			Dectcontect(&con);
			break;
		default:
			printf("选择错误，请重新选择！\n");
			break;
		}
	} while (input);
	return 0;
}