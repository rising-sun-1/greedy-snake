#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define MAX_CAP 3

//包含一个人的信息的结构体
typedef struct Peoinfor {
	char name[20];
	int age;
	char sex[5];
	char telephone[20];
	char addr[30];
}Peoinfor;

//通讯录结构体
typedef struct contect {
	Peoinfor* data;
	int sz;
	int capacity;
}contect;

//名字排序函数
int cmp_name(const void* p1, const void* p2);
//查找通讯录的某个联系人
int searchpeo(contect* con, char name[]);
//初始化通讯录
void Initcontect(contect* con);
//添加联系人信息
void Addcontect(contect* con);
//删除联系人的信息
void Deccontect(contect* con);
//打印联系人的信息
void Showcontect(contect* con);
//查找某个联系人的所有信息
void Seccontect(contect* con);
//修改某个联系人的信息
void Meycontect(contect* con);
//按照名字进行排序
void Sortbyname(contect* con);

//将信息输入文件中
void Filecontect(contect* con);

//将文件中的信息输入到内存中
void Readcontect(contect* con);





