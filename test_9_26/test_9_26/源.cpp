#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

void my_memcpy(void* dest, const void* src, size_t num)
{
	assert(dest && src);
	while (num--)
	{
		*(char*)dest = *(char*)src;
		dest = (char*)dest + 1;
		src = (char*)src + 1;
	}
}

//void my_memcpy(void* dest, const void* src, size_t num)
//{
//	assert(dest && src);
//	while (num--)
//	{
//		*(char*)dest = *(char*)src;
//		((char*)dest)++;
//		((char*)src)++;
//	}
//}

int main()
{
	int arr1[] = { 1,1,1,1,1,1,1,1,1 };
	int arr2[] = { 0,1,2,3,4 };
	memcpy(arr1, arr2, 5 * sizeof(int));
	for (int i = 0; i < 5; i++)
	{
		printf("%d ", arr1[i]);
	}
	return 0;
}




