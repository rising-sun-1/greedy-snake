#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

void my_memcpy(void* dest, const void* src, size_t num)
{
	assert(dest && src);
	while (num--)
	{
		*(char*)dest = *(char*)src;
		dest = (char*)dest + 1;
		src = (char*)src + 1;
	}
}

//void my_memmove(void* dest, const void* src, size_t num)
//{
//	if ((char*)dest < (char*)src)
//	{
//		while (num--)
//		{
//			*(char*)dest = *(char*)src;
//			dest = (char*)dest + 1;
//			src = (char*)src + 1;
//		}
//	}
//	else
//	{
//		while (num--)
//		{
//			*((char*)dest + num) = *((char*)src + num);
//		}
//	}
//}

//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,7,9,10 };
//	//my_memcpy(arr + 2, arr, 20);
//	my_memmove(arr + 2, arr, 20);
//	//memcpy(arr + 2, arr, 20);
//	return 0;
//}

//int main()
//{
//	int arr[10];
//	memset(arr, 0, sizeof(arr));
//	return 0;
//}


//int main()
//{
//	int arr1[] = { 1,2,3,4,5,6,7 };
//	int arr2[] = { 1,2,3,0x11223304 };
//	printf("%d", memcmp(arr1, arr2, 13));
//	return 0;
//}



#include <stdio.h>
// typedef struct chance{
//     int l;
//     int r;
// }chance;
int main()
{
    int a = 0, k = 0, q = 0;
    int l = 0, r = 0;
    scanf("%d %d %d", &a, &k, &q);
    while (scanf("%d %d", &l, &r) != EOF)
    {
        int count1 = 0;
        int count2 = 0;
        for (int i = l; i <= r; ++i)
        {
            if ((a + (i - 1) * k) % 2 == 0)
            {
                count1++;
            }
            else if ((a + (i - 1) * k) % 2 != 0)
            {
                count2++;
            }
        }
        if (count1 < count2)
        {
            printf("%d\n", 1);
        }
        else if (count1 > count2)
        {
            printf("%d\n", -1);
        }
        else
        {
            printf("%d\n", 0);
        }
    }
    return 0;
}