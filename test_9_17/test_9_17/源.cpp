#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <assert.h>
#include <string.h>
//模拟实现一下strcmp函数
//int my_strcmp(const char* str1, const char* str2)
//{
//	assert(str1 && str2);
//	while (*str1 && *str2 && *str1 == *str2)
//	{
//		if (*str2 == 0)
//			return 0;
//		str1++;
//		str2++;
//	}
//	if (*str1 < *str2)
//		return -1;
//	else if(*str1 > *str2)
//		return 1;
//}

int my_strcmp(const char* str1, const char* str2)
{
	assert(str1 && str2);
	while (*str1 && *str2 && *str1 == *str2)
	{
		str1++;
		str2++;
	}
	return *str1 - *str2;
}

//模拟实现一下strcat函数
char* my_strcat(char* str1, const char* str2)
{
	assert(str1 && str2);
	char* ret = str1;
	while (*str1 != 0)
	{
		str1++;
	}
	while (*str1++ = *str2++)
	{
		;
	}
	return ret;
}

//模拟实现一下strstr函数

const char* my_strstr(const char* str1, const char* str2)
{
	assert(str1 && str2);
	const char* s1;
	const char* s2;
	const char* cp;
	cp = str1;
	while (*cp)
	{
		s1 = cp;
		s2 = str2;
		while (*s1 == *s2)
		{
			if (*s2 == 0)
				return cp;
			s1++;
			s2++;
		}
		cp++;
	}
	return NULL;
}


int main()
{
	char arr1[20] = "sfwe";
	char arr2[] = "biu";
	printf("%d", my_strcmp(arr1, arr2));
	return 0;
}