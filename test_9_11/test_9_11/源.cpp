#define _CRT_SECURE_NO_WARNINGS

#include "heapsort.h"

int main()
{
	int arr[] = { 2,4,5,6,0,9,4,5,6,1 };
	int numsSize = sizeof(arr) / sizeof(arr[0]);
	heapsort(arr, numsSize);
	for (int i = 0; i < numsSize; ++i)
	{
		printf("%d", arr[i]);
	}
	return 0;
}