#pragma once
#include <stdio.h>


void heapsort(int* nums, int numsSize);

void heapIndex(int* nums, int index);

void heapify(int* nums, int index, int numsSize);

void swap(int* nums, int a, int b);