#define _CRT_SECURE_NO_WARNINGS


#include <stdio.h>

int main()
{
	char arr[21] = { 0 };
	int i = 0;
	do
	{
		scanf("%c", &arr[i]);
		i++;
	} while (arr[i] != '\n');

	printf("%s\n", arr);
	return 0;
}