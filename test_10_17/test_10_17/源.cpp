#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>

int main()
{
	int T = 0;
	scanf("%d", &T);
	for (int i = 0; i < T; i++) {
		int a = 0, b = 0, c = 0, n = 0;
		scanf("%d %d %d %d", &a, &b, &c, &n);
		if ((a + b + c + n) / 3 == 0) {
			printf("YES\n");
		}
		else
			printf("NO\n");
	}
	return 0;
}


#include <stdio.h>

int main()
{
	int T = 0;
	scanf("%d", &T);
	for (int i = 0; i < T; i++) {
		int a = 0, b = 0, c = 0, n = 0;
		scanf("%d %d %d %d", &a, &b, &c, &n);
		if ((a + b + c + n) % 3)
			printf("NO\n");
		else {
			int temp = (a + b + c + n) / 3;
			if (a > temp || b > temp || c > temp)
				printf("NO\n");
			else
				printf("YES\n");
		}
	}
	return 0;
}