#define _CRT_SECURE_NO_WARNINGS


#include <iostream>
#include <string>
using namespace std;


int main()
{
	//取消同步流
	ios::sync_with_stdio(0), cin.tie(0), cout.tie(0);
	//声明一个string类
	string str1;
	//使用字符串常量初始化字符串
	string str2 = "hello, world";
	//使用另一个string对象来初始化字符串
	string str3 = str2;
	//使用部分字符串来初始化数组
	string str4 = str2.substr(0, 5);

	string str5(5, 'A');
	const char* Arr = "hello";
	string str6(Arr);
	return 0;
}