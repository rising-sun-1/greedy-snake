#define _CRT_SECURE_NO_WARNINGS

//#include <stdio.h>
//int main()
//{
//	int T = 0;
//	scanf("%d", &T);
//	for (int i = 0; i < T; i++) {
//		int x = 0, y = 0, z = 0;
//		scanf("%d %d %d", &x, &y, &z);
//		int ans = ((z - y) / x) * x + y;
//		printf("%d\n", ans);
//	}
//	return 0;
//}

//#include <stdio.h>
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int arr[100][100];
//	for (int i = 0; i < n; i++) {
//		for (int j = 0; j < n; j++) {
//			scanf("%d", &arr[i][j]);
//		}
//	}
//	int count = 0;
//	int a[10001];
//	for (int i = 0; i < n; i++) {
//		for (int j = 0; j < n; j++) {
//			if (arr[i][j] != -1 || arr[i][j] != 1 || arr[i][j] != 3) {
//				a[count] = i + 1;
//				count++;
//			}
//	/*	}
//	}
//	if (count == 0) {
//		printf("0\n");
//		return -1;
////	}
//	else {
//		printf("%d\n", count);
//		for (int i = 0; i < count; i++) {
//			printf("%d ", a[i]);
//		}
//		printf("\n");
//	}
//
//	return 0;
//}*/

//#include <stdio.h>
//
//int main()
//{
//	FILE* pf = fopen("test1.txt", "wb");
//	int b = 10000;
//	fwrite(&b, 4, 1, pf);
//
//
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}



#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main()
{
	int t = 0;
	scanf("%d", &t);
	int* ans = (int*)malloc(sizeof(int) * t);
	memset(ans, 0, sizeof(ans));
	int k = 0;
	for (int i = 0; i < t; i++) {
		char x[50];
		char s[50];
		int a = 0;
		int b = 0;
		scanf("%d %d", &a, &b);
		scanf("%s", x);
		scanf("%s", s);
		int count = 0;
		int flag = 0;
		int n = strlen(x);
		while (n <= 50) {
			if (strstr(x, s) != NULL) {
				ans[k++] = count;
				flag = 1;
				break;
			}
			strcpy(x, strncat(x, x, n));
			n *= 2;
			count++;
		}
		if (flag == 0) {
			ans[k++] = -1;
		}
	}
	for (int i = 0; i < k; i++) {
		printf("%d\n", ans[i]);
	}
	return 0;
}