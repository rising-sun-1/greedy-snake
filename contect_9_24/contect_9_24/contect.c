#define _CRT_SECURE_NO_WARNINGS
#include "contect.h"
int cmp_name(const void* p1, const void* p2)
{
	return strcmp(((struct Peoinfor*)p1)->name, ((struct Peoinfor*)p2)->name);
}

void Initcontect(contect* con)
{
	memset(con->data, 0, sizeof(con->data));
	con->sz = 0;
}

int searchpeo(contect* con, char name[])
{
	assert(con);
	for (int i = 0; i < con->sz; i++)
	{
		if (strcmp(con->data[i].name, name) == 0)
		{
			return i;
		}
	}
	return -1;
}

void Addcontect(contect* con)
{
	assert(con);
	int num = 3;
	/*if (con->sz >= 100)
	{
		printf("通讯录已满，不能继续增加！\n");
		return;
	}*/
	if (con->sz == num) {
		realloc(con->data, sizeof(*con->data) * 3);
		num += 3;
		printf("通讯录已满，已经扩容通讯录！\n");
		return;
	}
	printf("请添加联系人的信息:\n");
	printf("联系人的名字:>");
	scanf("%s", con->data[con->sz].name);
	printf("联系人的年龄:>");
	scanf("%d", &(con->data[con->sz].age));
	printf("联系人的性别:>");
	scanf("%s", con->data[con->sz].sex);
	printf("联系人的电话:>");
	scanf("%s", con->data[con->sz].telephone);
	printf("联系人的地址:>");
	scanf("%s", con->data[con->sz].addr);
	con->sz++;
}

void Deccontect(contect* con)
{
	assert(con);
	if (con->sz == 0)
	{
		printf("通讯录为空，无需删除!\n");
		return;
	}
	char name[20];
	printf("请输入你要删除联系人的名字:>");
	scanf("%s", name);
	int size = searchpeo(con, name);
	if (size == con->sz - 1)
	{
		con->sz--;
	}
	if (size == -1)
	{
		printf("你要删除的联系人不存在！\n");
		return;
	}
	for (int i = size; i < con->sz - 1; i++)
	{
		con->data[i] = con->data[i + 1];
	}
	con->sz--;
	printf("删除成功！\n");
}

void Seccontect(contect* con)
{
	assert(con);
	if (con->sz == 0)
	{
		printf("通讯录为空，无需查找！\n");
		return;
	}
	char name[20];
	printf("请输入你要查找联系人的名字:>");
	scanf("%s", name);
	int size = searchpeo(con, name);
	if (size == -1)
	{
		printf("你要查找的联系人的信息不存在!\n");
		return;
	}
	printf("%-20s%-5s%-5s%-20s%-30s\n", "名字", "年龄", "性别", "电话", "地址");
	int i = size;
	printf("%-20s%-5d%-5s%-20s%-30s\n",
		con->data[i].name, con->data[i].age, con->data[i].sex,
		con->data[i].telephone, con->data[i].addr);
}

void Meycontect(contect* con) //修改联系人的信息
{
	assert(con);
	if (con->sz == 0)
	{
		printf("通讯录为空，无需查找！\n");
		return;
	}
	char name[20];
	printf("请输入你要修改联系人的名字:>");
	scanf("%s", name);
	int size = searchpeo(con, name);
	if (size == -1)
	{
		printf("你要查找的联系人的信息不存在!\n");
		return;
	}
	printf("请修改联系人的信息:\n");
	printf("联系人的名字:>");
	scanf("%s", con->data[size].name);
	printf("联系人的年龄:>");
	scanf("%d", &(con->data[size].age));
	printf("联系人的性别:>");
	scanf("%s", con->data[size].sex);
	printf("联系人的电话:>");
	scanf("%s", con->data[size].telephone);
	printf("联系人的地址:>");
	scanf("%s", con->data[size].addr);
}

void Showcontect(contect* con)
{
	assert(con);
	printf("%-20s%-5s%-5s%-20s%-30s\n", "名字", "年龄", "性别", "电话", "地址");
	for (int i = 0; i < con->sz; i++)
	{
		printf("%-20s%-5d%-5s%-20s%-30s\n",
			con->data[i].name, con->data[i].age, con->data[i].sex,
			con->data[i].telephone, con->data[i].addr);
	}
}

void Sortbyname(contect* con)
{
	qsort(&(con->data), con->sz, sizeof(con->data[0]), cmp_name);
	printf("排好序的结构如下:\n");
	printf("%-20s%-5s%-5s%-20s%-30s\n", "名字", "年龄", "性别", "电话", "地址");
	for (int i = 0; i < con->sz; i++)
	{
		printf("%-20s%-5d%-5s%-20s%-30s\n",
			con->data[i].name, con->data[i].age, con->data[i].sex,
			con->data[i].telephone, con->data[i].addr);
	}
}
