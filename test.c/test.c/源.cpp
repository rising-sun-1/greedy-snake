#define _CRT_SECURE_NO_WARNINGS

//#include <stdio.h>
//
//int main()
//{
//	int T = 0;
//	scanf("%d", &T);
//	for (int i = 0; i < T; i++) {
//		int n = 0;
//		scanf("%d", &n);
//		for (int j = 1; j <= n; j++) {
//			printf("%d ", j);
//		}
//		printf("\n");
//	}
//	return 0;
////}
//
//#include <stdio.h>
//#include <string.h>
//#include <math.h>
//#include <stdlib.h>

//int main()
//{
//	char arr[100];
//	scanf("%s", arr);
//	int len = strlen(arr);
//	int num = 0;
//	num = atoi(arr);
//	//3454   第一个完整数
//	//找出有一个数可以整除8
//	//900000000000000000000000 asz7845xzdfv123rgtrt23rt23rt23rt23rt23iu
//	//排列组合出3 4 5 4  
//	// 
//	//能被8整除，等价于后3位可以被8整除
//	//能被2或5整除，等价于后一位可以被2或5整除
//	//能被4整除，等价于后2位可以被4整除
//	//能被3或9整除 等价于各位数字之和能被3或9整除
//	//能被11整除，等价于奇数位各位数字之和减去偶数位各位数字之和的差值能被11整除
//	//能被7或13整除，等价于后三位之前的数减去后三位的差值可以被7或13整除
//	//344    
//	//345   
//	//354
//	//34
//	//35
//	//3 4 5 4 单独分析，
//	if (num % 8 == 0) {
//		printf("YES\n");
//		printf("%d\n", num);
//		return 1;
//	}
//	for (int i = 0; i < len; i++) {
//		if ((arr[i] - '0') % 8 == 0) { //n
//			printf("YES\n");
//			printf("%d\n", arr[i] - '0');
//			return 1;
//		}
//	}
//	for (int i = 0; i < len; i++) {
//		for (int j = 0; j < len; j++) {
//			
//		}
//	}
//	return 0;
//}

#include <iostream>
#include <string>
#include <cstring>
#include <algorithm>

using namespace std;

int main()
{
	string s;
	scanf("%s", s);

	s = "00" + s;
	for (int i = 0; i < s.size(); i++) {
		for (int j = i + 1; j < s.size(); j++) {
			for (int k = j + 1; k < s.size(); k++) {
				int num = (s[i] * 100 + s[j] * 10 + s[k] * 1 - 111 * '0');
				if (num % 8 == 0) {
					printf("YES\n");
					printf("%d\n", num);
					return 0;
				}
			}
		}
	}
	printf("NO\n");
	return 0;
}