#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <iostream>
#include <math.h>
using namespace std;

int is_sushu(int n)
{
    int i = 0;
    for (i = 2; i <= n; i++)
    {
        if (n % i == 0)
            break;
    }
    if (n == i)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

int main()
{
    int n, k;
    // 请在此添加代码，输入n和k，并输出n以内k个素数以及它们的和
    /********** Begin *********/
    scanf("%d %d", &n, &k);
    int arr[100000];
    int count = 0;
    for (int j = n; j >= 0; j--)
    {
        if (is_sushu(j))
        {
            arr[count++] = j;
        }
    }
    int ans = 0;
    if (count < k)
    {
        k = count;
    }
    for (int i = 0; i < k; i++)
    {
        ans += arr[i];
    }
    for (int i = 0; i < k; i++)
    {
        printf("%d ", arr[i]);
    }
    printf("%d\n", ans);
    /********** End **********/

    return 0;
}


#include <stdio.h>
#include <iostream>
using namespace std;

int main()
{
    int n;

    // 请在此添加代码，输入正整数n，如果n是素数则输出“Yes”，否则输出“No”
    /********** Begin *********/
    scanf("%d", &n);
    int i = 0;
    for (i = 2; i <= n; i++)
    {
        if (n % i == 0)
        {
            break;
        }
    }
    if (n == i)
    {
        printf("Yes");
    }
    else {
        printf("No");
    }

    /********** End **********/

    return 0;
}

#include <stdio.h>
#include <iostream>
#include <malloc.h>
using namespace std;

int fsort(int arr[], int n)
{
    for (int i = 0; i < n - 1; i++)
    {
        for (int j = 0; j < n - 1 - i; j++)
        {
            if (arr[j] < arr[j + 1])
            {
                int temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }
        }
    }
    int ret = 0;
    int k = 1;
    for (int i = 0; i < n; i++)
    {
        ret += arr[i] * k;
        k *= 10;
    }
    return ret;
}

int esort(int arr[], int n)
{
    for (int i = 0; i < n - 1; i++)
    {
        for (int j = 0; j < n - 1 - i; j++)
        {
            if (arr[j] > arr[j + 1])
            {
                int temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }
        }
    }
    int ret = 0;
    int k = 1;
    for (int i = 0; i < n; i++)
    {
        ret += arr[i] * k;
        k *= 10;
    }
    return ret;
}

int* sarr(int arr[], int num)
{
    for (int i = 0; i < 3; i++)
    {
        arr[i] = 0;
    }
    for (int i = 0; i < 3; i++)
    {
        arr[i] = num % 10;
        num /= 10;
    }
    return arr;
}


int main()
{
    int n;
    // 请在此添加代码，输出整数进入黑洞过程
    /********** Begin *********/
    scanf("%d", &n);
    int temp = n;
    int* arr1 = (int*)malloc(sizeof(int) * 3);
    arr1 = sarr(arr1, temp);
    int max = esort(arr1, 3);
    int min = fsort(arr1, 3);
    int ans = max - min;
    int i = 1;
    while (max - min != 495)
    {
        printf("%d:%d-%d=%d\n", i, max, min, ans);
        arr1 = sarr(arr1, ans);
        max = esort(arr1, 3);
        min = fsort(arr1, 3);
        ans = max - min;
        i++;
    }
    printf("%d:%d-%d=%d\n", i, max, min, ans);

    /********** End **********/
    return 0;
}

#include <stdio.h>
#include <iostream>
using namespace std;
// int weishu(int n)
// {
//     int count = 0;
//     int ret = n;
//     while(ret)
//     {
//         ret /= 10;
//         count++;
//     }
//     return count;
// }

void sort(int arr[], int num)
{
    for (int i = 0; i < num - 1; i++)
    {
        for (int j = 0; j < num - 1 - i; j++)
        {
            if (arr[j] < arr[j + 1])
            {
                int temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }
        }
    }
}

int main()
{
    // n-输入的数，m-重排后的数
    int n, m;
    // 请在此添加代码，输入一个小于1000的正整数，重排出最大的数存入m中
    /********** Begin *********/
    scanf("%d", &n);
    int temp1 = n;
    int arr[3];
    for (int i = 0; i < 3; i++)
    {
        arr[i] = 0;
    }
    for (int i = 0; i < 3; i++)
    {
        arr[i] = temp1 % 10;
        temp1 /= 10;
    }
    int max = 1;
    sort(arr, 3);
    for (int i = 0; i < 3; i++)
    {
        printf("%d", arr[i]);
    }
    /********** End **********/
    // 输出重排后的数
    //cout << m << endl;
    return 0;
}
