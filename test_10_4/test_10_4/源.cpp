﻿#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stddef.h>
//struct Node {
//	int a;
//	struct Node n;
//};

typedef struct Node {
	int a;
	struct Node* next;
}Node;




struct Point
{
	int x;
	int y;
}p1 = { 1,2 }; //声明类型的同时定义变量p1,并初始化p1

struct Point p2 = { 2,4 }; //定义结构体变量p2

//初始化：定义变量的同时赋初值。
//int x = 20;
//int y = 40;
//struct Point p3 = { x, y };
//
//struct Stu        //类型声明
//{
//	char name[15];//名字
//	int age;      //年龄
//};
//struct Stu s = { "zhangsan", 20 };//初始化
//struct Stu s1 = { .age = 19, .name = "hskod" };
//struct Node
//{
//	int data;
//	struct Point p;
//	struct Node* next;
//}n1 = { 10, {4,5}, NULL }; //结构体嵌套初始化
//struct Node n2 = { 20, {5, 6}, NULL };//结构体嵌套初始化

struct Stu1 {
	int a;
	char c1;
	char c2;
};

struct Stu2 {
	char c1;
	int a;
	char c2;
};

int main()
{
	/*printf("%d\n", sizeof(struct Stu1));
	printf("%d\n", sizeof(struct Stu2));*/
	printf("%d\n", offsetof(struct Stu2, c1));
	printf("%d\n", offsetof(struct Stu2, a));
	printf("%d\n", offsetof(struct Stu2, c2));
	return 0;
}