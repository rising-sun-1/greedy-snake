#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>


//部分可以进行ac，
int main()
{
	int n = 0, k = 0, q = 0;
	int arr[100010];
	memset(arr, 0, sizeof(arr));  //将数组元素初始化为0
	scanf("%d %d %d", &n, &k, &q);
	for (int i = 0; i < n; i++) {
		long long l = 0, r = 0;
		scanf("%d %d", &l, &r);  //输入区间的范围
		for (int j = l; j <= r; j++) {
			arr[j]++;  //将出现的数字进行相加
		}
	}
	for (int i = 0; i < q; i++) {
		int a = 0, b = 0;
		scanf("%d %d", &a, &b);  //要考验的区间
		int ans = 0;
		for (int j = a; j <= b; j++) {
			if (arr[j] == k)
				ans++;
		}
		printf("%d\n", ans);
	}
	return 0;
}