#pragma once
#include "sprite.h"
#include <easyx.h>

struct background {
	struct sprite super;
	int yA;
	int yB;
	IMAGE* imgBackground;
};

//void backgroundDraw(struct background* b);
//void backgroundUpdate(struct background* b);
void backgroundInit(struct background* b);
void backgroundDestroy(struct background* b);
