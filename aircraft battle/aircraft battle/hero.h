#pragma once
#include <stdio.h>
#include <easyx.h>
#include "sprite.h"

enum heroStatus {
	hero_normal0,
	hero_normal1,
	hero_down0,
	hero_down1,
	hero_down2,
	hero_down3,
	hero_destroy
};


struct hero {
	struct sprite super;
	IMAGE* imgarrHero[6];
	IMAGE* imgarrHeroMask[6];
	enum heroStatus status;
	int life;           //记录英雄的生命值
	int heroUpdateCnt;  //更新计数器
};

void heroInit(struct hero* h);
void heroDestroy(struct hero* h);
