#define _CRT_SECURE_NO_WARNINGS

#include "sprite.h"
#include "gameloop.h"

//
void gameloop(sprite* s, int fps)
{
	timeBeginPeriod(1);
	LARGE_INTEGER startCount, endCount, F;
	QueryPerformanceFrequency(&F);

	BeginBatchDraw();
	while (1)
	{
		QueryPerformanceCounter(&startCount);
		cleardevice();

		s->draw(s);
		s->update(s);

		QueryPerformanceCounter(&endCount);
		long long elapse = (endCount.QuadPart - startCount.QuadPart) / F.QuadPart * 1000000;

		while (elapse < 1000000 / fps)
		{
			Sleep(1);
			QueryPerformanceCounter(&endCount);
			elapse = (endCount.QuadPart - startCount.QuadPart) / F.QuadPart * 1000000;
		}
		FlushBatchDraw();
	}
	EndBatchDraw();
	timeEndPeriod(1);
}