#define _CRT_SECURE_NO_WARNINGS
#include "hero.h"
#include "image.h"
#include "sprite.h"

enum heroStatus heroStatusSqauence[7] = {
	hero_normal0,
	hero_normal1,
	hero_down0,
	hero_down1,
	hero_down2,
	hero_down3,
	hero_destroy
};

void heroDraw(struct hero* h)
{
	putTransparentImage(h->super.x, h->super.y, h->imgarrHeroMask[h->status], h->imgarrHero[h->status]);
}

void heroUpdate(struct hero* h)
{
	h->heroUpdateCnt++;
	if (h->heroUpdateCnt >= 15)
	{
		h->heroUpdateCnt = 0;
		//如果英雄飞机的血量不等于0，则进行切换状态
		if (h->life != 0)
		{
			if (h->status == hero_normal0)
				h->status = hero_normal1;
			else if (h->status == hero_normal1)
				h->status = hero_normal0;
		}
		else
		{
			if (h->status < hero_destroy)
				h->status = heroStatusSqauence[h->status + 1];
		}
	}
}

//用于初始化hero对象的
void heroInit(struct hero* h)
{
	h->super.draw = (void (*)(struct sprite*))heroDraw;
	h->super.update = (void (*)(struct sprite*))heroUpdate;

	h->heroUpdateCnt = 0;
	h->status = hero_normal0;
	h->life = 1;

	h->super.x = 178;
	h->super.y = 600;

	for (int i = 0; i < 6; i++)
	{
		h->imgarrHero[i] = new IMAGE;
		h->imgarrHeroMask[i] = new IMAGE;
	}
	char imgPath[50];
	char imgMaskPath[50];
	for (int i = 0; i < 2; i++)
	{
		sprintf(imgPath, "img/hero/hero%d.png", i);
		sprintf(imgMaskPath, "img/hero/hero%d_mask.png", i);
		loadimage(h->imgarrHero[i], imgPath);
		loadimage(h->imgarrHeroMask[i], imgMaskPath);
	}
	for (int i = 0; i < 4; i++)
	{
		sprintf(imgPath, "img/hero/hero%d.png", i);
		sprintf(imgMaskPath, "img/hero/hero%d_mask.png", i);
		loadimage(h->imgarrHero[i + 2], imgPath);
		loadimage(h->imgarrHeroMask[i + 2], imgMaskPath);
	}
}

void heroDestroy(struct hero* h)
{
	for (int i = 0; i < 6; i++)
	{
		delete h->imgarrHero[i];
		delete h->imgarrHeroMask[i];
	}
}