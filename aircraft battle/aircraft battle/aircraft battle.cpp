﻿#include "hero.h"
#include "gameloop.h"
#include "background.h"
int main()
{
	initgraph(422, 750);
	setbkcolor(WHITE);
	cleardevice();

	/*hero h;
	heroInit(&h);
	gameloop((sprite *)&h, 60);
	heroDestroy(&h);*/

	background b;
	backgroundInit(&b);
	gameloop((sprite*)&b, 60);
	backgroundDestroy(&b);

	closegraph();
	return 0;
}