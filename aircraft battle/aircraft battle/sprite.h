#pragma once
#include <easyx.h>

//精灵的一些共性
struct sprite {
	void (*draw)(sprite*);    //绘制画面
	void (*update)(sprite*);  //更新数据
	int x;
	int y;
	int height;
	int width;
};