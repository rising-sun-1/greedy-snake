#define _CRT_SECURE_NO_WARNINGS


#include <bits/stdc++.h>
using namespace std;

typedef pair<int, int> PII;
typedef long long ll;
const int N = 1500;
int n, res;
vector<PII> vc;
ll p[N];

int main()
{
	cin >> n;
	int a = 0, b = 0;
	cin >> a >> b;
	vc.push_back({ a, b });

	for (int i = 1; i <= n; i++) {
		cin >> a >> b;
		vc.push_back({ a, b });
	}

	sort(vc.begin() + 1, vc.end());

	ll menoy = 0;
	p[0] = vc[0].first;
	for (int i = 1; i <= n; i++) {
		p[i] = p[i - 1] * vc[i].first; //�洢����ǰ׺�� 
	}

	for (int i = 1; i <= n; i++) {
		menoy = max(menoy, p[i - 1] / vc[i].second);
	}

	cout << menoy << '\n';

	return 0;
}