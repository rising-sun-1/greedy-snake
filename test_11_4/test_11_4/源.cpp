#define _CRT_SECURE_NO_WARNINGS


#include <bits/stdc++.h>
using namespace std;

const int N = 100010;

int n, m;
int a[N], cnt;

void down(int x) {
    int t = x;
    if (x * 2 <= cnt && a[x * 2] < a[t])  t = x * 2;
    if (x * 2 + 1 <= cnt && a[x * 2 + 1] < a[t])  t = x * 2 + 1;
    if (x != t) {
        swap(a[x], a[t]);
        down(t);
    }
}

int main()
{
    cin >> n >> m;
    for (int i = 1; i <= n; i++) {
        cin >> a[i];
    }
    cnt = n;

    for (int i = n / 2; i >= 1; i--) {
        down(i);
    }

    while (m--) {
        cout << a[1] << ' ';
        a[1] = a[cnt];
        cnt--;
        down(1);
    }
    puts("");
    return 0;
}