#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <assert.h>
//模拟实现strlen函数
size_t my_strlen(const char* str)
{
	int count = 0;
	assert(str);
	while (*str++)
	{
		count++;
	}
	return count;
}

//模拟实现strcpy函数
char* my_strcpy(char* base, const char* serc)
{
	assert(base && serc);
	char* ans = base;
	while (*base++ = *serc++)
	{
		;
	}
	return ans;
}

int main()
{
	char arr[] = "daspfoe";
	char arr1[20];
	char arr2[] = "absdf";
	printf("%s\n", my_strcpy(arr1, arr2));
	printf("%zd\n", my_strlen(arr));
	return 0;
}