#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
//void test()
//{
//    printf("Hellod,worid!");
//}
//
//int main()
//{
//    printf("%p\n", test);   //函数与数组类似，数组名表示数组首元素的地址，函数名表示函数的地址
//    printf("%p\n", &test);  //&函数名拿到的是函数的地址
//}

//void test()
//{
//    printf("Hellod,worid!\n");
//}
//
//int main()
//{
//    void (*pf)() = &test;
//    (*pf)();
//    pf();
//    test();
//}

void menu()
{
	printf("***************************\n");
	printf("***** 1. Add   2. Sub *****\n");
	printf("***** 3. Mul   4. Div *****\n");
	printf("*****      0. exit    *****\n");
	printf("***************************\n");
}

int Add(int x, int y)
{
	return x + y;
}
int Sub(int x, int y)
{
	return x - y;
}
int Mul(int x, int y)
{
	return x * y;
}
int Div(int x, int y)
{
	return x / y;
}
//int main()
//{
//	int input = 0;
//	int x = 0;
//	int y = 0;
//	int ret = 0;
//	do {
//		menu();
//		printf("请选择:>");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
			/*printf("请输入两个数:>");
			scanf("%d %d", &x, &y);
			ret = Add(x, y);
			printf("结果是:> %d\n", ret);*/
//			break;
//		case  2:
//			printf("请输入两个数:>");
//			scanf("%d %d", &x, &y);
//			ret = Sub(x, y);
//			printf("结果是:> %d\n", ret);
//			break;
//		case 3:
//			printf("请输入两个数:>");
//			scanf("%d %d", &x, &y);
//			ret = Mul(x, y);
//			printf("结果是:> %d\n", ret);
//			break;
//		case 4:
//			printf("请输入两个数:>");
//			scanf("%d %d", &x, &y);
//			ret = Div(x, y);
//			printf("结果是:> %d\n", ret);
//			break;
//		case 0:
//			printf("退出计算器！\n");
//			break;
//		default:
//			printf("选择错误，请重新选择！\n");
//			break;
//		}
//
//	} while (input);
//	return 0;
//}


//int main()
//{
//	int input = 0;
//	int x = 0;
//	int y = 0;
//	int ret = 0;
//	do {
//		menu();
//		printf("请选择:>");
//		scanf("%d", &input);
//		int (*pfarr[])(int, int) = { NULL, &Add, &Sub, &Mul, &Div };
//		if (0 == input)
//		{
//			printf("退出计算器！\n");
//		}
//		else if (input >= 1 && input <= 4)
//		{
//			printf("请输入两个数:>");
//			scanf("%d %d", &x, &y);
//			ret = (*pfarr[input])(x, y);
//			printf("结果是:> %d\n", ret);
//		}
//		else
//		{
//			printf("选择错误，请重新选择!\n");
//		}
//	} while (input);
//	return 0;
//}
void test(const char* str)
{
	printf("%s\n", str);
}

int main()
{
	//函数指针pf
	void (*pf)(const char*) = test;
	//函数指针数组pfarr
	void (*pfarr[10])(const char*);
	pfarr[0] = test;
	//指向函数指针数组pfarr的指针ppfarr
	void (*(*ppfarr)[10])(const char*) = &pfarr;
	return 0;
}


//输入一个字符串和一个整数 k ，截取字符串的前k个字符并输出

#include <stdio.h>
int main()
{
	int k = 0;
	char arr[1010];
	scanf("%s", arr);
	scanf("%d", &k);
	for (int i = 0; i < k; i++)
	{
		printf("%c", arr[i]);
	}
	printf("\n");
	return 0;
}