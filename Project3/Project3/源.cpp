#define _CRT_SECURE_NO_WARNINGS

//#include <iostream>
//
//using namespace std;
//
//const int N = 1000010;
//int a[N];
//
//void quicksort(int a[], int l, int r) {
//    if (l >= r)  return;
//
//    int i = l - 1, j = r + 1, q = a[l + r >> 1];
//    while (i < j) {
//        do  i++; while (a[i] < q);
//        do  j--; while (a[j] > q);
//        if (i < j)  swap(a[i], a[j]);
//    }
//
//    quicksort(a, l, j);
//    quicksort(a, j + 1, r);
//}
//
//int main()
//{
//    int n = 0;
//    scanf("%d", &n);
//    for (int i = 0; i < n; i++) {
//        scanf("%d", &a[i]);
//    }
//
//    quicksort(a, 0, n - 1);
//
//    for (int i = 0; i < n; i++) {
//        printf("%d ", a[i]);
//    }
//    return 0;
//}


int max(int a, int b) {
    return a > b ? a : b;
}

int maxArea(int* height, int heightSize) {
    int res = 0, ares = 0;
    int i = 0, j = heightSize - 1;
    while (i < j) {
        if (height[i] > height[j]) {
            ares = height[j] * (j - i);
            res = max(res, ares);
        }
        else {
            ares = height[i] * (j - i);
            res = max(res, ares);
        }
        if (height[i] < height[j]) {
            i++;
        }
        else {
            j--;
        }
    }
    return res;
}