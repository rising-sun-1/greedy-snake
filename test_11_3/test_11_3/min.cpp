#define _CRT_SECURE_NO_WARNINGS

#include <bits/stdc++.h>
using namespace std;

const int N = 100010;
int n, m;
int p[N];  //表示这个数字的父节点是什么

//查并集合是高效地检查两个数字是否在一个集合中，并且将两个集合进行合并
int find(int x) {
    if (p[x] != x) {  //如果说父节点不等于本身，那么这个结点不是祖宗结点
        p[x] = find(p[x]);  //就利用递归进行继续查找，并在查找的过程中将数据压缩
    }
    return p[x]; // 最后返回这个父节点即可
}


int main()
{
    scanf("%d %d", &n, &m);

    for (int i = 1; i <= n; i++) {
        p[i] = i; // 这个数字的头结点是本身
    }

    char s[2];
    while (m--) {
        int a = 0, b = 0;
        scanf("%s %d %d", s, &a, &b);
        if (s[0] == 'M') {//M是合并，如果两个数字的祖宗结点不一样的话，说明他们在不同的集合中，如果一样的话，说明他们在一个集合中
            //将这个一个数字的祖宗结点当成父节点，将其链接起来
            if (find(a) != find(b)) {
                p[find(a)] = find(b); //各自的这个父节点进行合并
            }
        }
        else {
            if (find(a) == find(b)) {
                puts("Yes");
            }
            else {
                puts("No");
            }
        }
    }

    return 0;
}



#include <bits/stdc++.h>
using namespace std;

const int N = 200020, M = 1000010;

int n;
int son[M][26], idx, cnt[M];
char str[M];

void Insert(char* str) {
    int p = 0; // 刚开始的父节点定义为0,
    for (int i = 0; str[i]; i++) {
        int u = str[i] - 'a';
        if (!son[p][u]) {
            son[p][u] = ++idx; // idx的作用是指明下标，
        }
        p = son[p][u];
    }

    cnt[p]++;
}

int Find(char* str) {
    int p = 0;
    for (int i = 0; str[i]; i++) {
        int u = str[i] - 'a';
        if (!son[p][u]) {
            return 0;
        }
        p = son[p][u];
    }
    return cnt[p];
}
int main()
{
    scanf("%d", &n);
    while (n--) {
        char s[5];
        scanf("%s %s", s, str);
        if (s[0] == 'I') {
            Insert(str);
        }
        else {
            int ans = Find(str);
            printf("%d\n", ans);
        }
    }
    return 0;
}