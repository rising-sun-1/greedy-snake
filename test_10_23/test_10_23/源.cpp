#define _CRT_SECURE_NO_WARNINGS


#define N 10100

struct stark {
    int size;
    int left;
    int right;
    int limit;
    char arr[N];
};
//��ʼ��ջ
void initstark(struct stark* obj, int k) {
    obj->size = 0;
    obj->left = 0;
    obj->right = 0;
    obj->limit = k;
}


//�ж�ջ�Ƿ�Ϊ��
bool isfull(struct stark* obj) {
    if (obj->size == obj->limit) {
        return true;
    }
    return false;
}


//�ж�ջ�Ƿ�Ϊ��
bool isempty(struct stark* obj) {
    if (obj->size == 0) {
        return true;
    }
    return false;
}

//����ջ��Ԫ��
bool instark(struct stark* obj, char ch) {
    if (isfull(obj)) {
        return false;
    }
    else {
        obj->arr[obj->right] = ch;
        obj->right += 1;
        obj->size++;
        return true;
    }
}

//����ջ��һ��Ԫ��
char outstark(struct stark* obj) {
    if (isempty(obj)) {
        return " ";
    }
    else {
        obj->size--;
        obj->right -= 1;
        return obj->arr[obj->right];
    }
}

bool isValid(char* s) {
    struct stark* obj = (struct srtark*)malloc(sizeof(struct stark));
    int len = strlen(s);
    initstark(obj, len);
    for (int i = 0; i < len; i++) {
        if (s[i] == '(' || s[i] == '{' || s[i] == '[') {
            instark(obj, s[i]);
        }
        else {
            if (isempty(obj)) {
                return false;
            }
            if (s[i] == ')' && outstark(obj) != '(') {
                return false;
            }
            if (s[i] == '}' && outstark(obj) != '{') {
                return false;
            }
            if (s[i] == ']' && outstark(obj) != '[') {
                return false;
            }
        }
    }
    return isempty(obj);
}