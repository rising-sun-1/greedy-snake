#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <string.h>
//3刷题统计
//int main()
//{
//	int a = 0, b = 0, n = 0;
//	scanf("%d %d %d", &a, &b, &n);
//	int ans = 0;
//	int temp = n / (5 * a + 2 * b); //包含了多少个星期
//	ans += temp * 7;
//	int div = n - temp * (5 * a + 2 * b);
//	if (div <= 5 * a)//只在星期一到星期五中有
//	{
//		ans += div / a;
//		ans++;
//	}
//	else
//	{
//		ans = ans + 5 + (div - 5 * a) / b;
//		ans++;
//	}
//	printf("%d\n", ans);
//	return 0;
//}

//4修剪灌木
//返回最小值
//int max(int num1, int num2)
//{
//	return num1 > num2 ? num1 : num2;
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	for (int i = 0; i < n; ++i)
//	{
//		int x = max(i, n - i - 1);
//		printf("%d\n", x * 2);
//	}
//	return 0;
//}

//5x进制减法
//#include <stdio.h>
//#include <string.h>
int max(int num1, int num2)
{
	return num1 > num2 ? num1 : num2;
}

int main()
{
	int N = 0; //最高进制
	scanf("%d", &N);
	int arr1[100];
	int arr2[100];
	memset(arr1, 0, sizeof(arr1));
	memset(arr2, 0, sizeof(arr2));
	int M1 = 0;
	scanf("%d", &M1);
	for (int i = 0; i < M1; i++)
	{
		scanf("%d", arr1 + i);
	}
	int M2 = 0;
	scanf("%d", &M2);
	for (int i = 0; i < M2; i++)
	{
		scanf("%d", arr2 + i);
	}
	int ans = 0;
	int flag = 1;
	for (int i = M1 - 1; i >= 0; i--)
	{
		if (arr1[i] - arr2[i] < 0)
		{
			ans += (arr1[i] - arr2[i]) * N;
		}
		else
		{
			ans += (arr1[i] - arr2[i]) * (flag);
			int temp = 0;
			if (max(arr1[i], arr2[i]) < 2)
			{
				temp = 2;
			}
			else
			{
				temp = max(arr1[i], arr2[i]) + 1;
			}
			flag *= temp;
		}
	}
	printf("%d", ans);
	return 0;
}