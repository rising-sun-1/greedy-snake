#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
//atoi函数是将数字字符串转换为整数

//int atoi(const char* str)
//
//int my_atoi(char* str) {
//	char* p1;
//	p1 = str;
//	while (*p1 == ' ') {
//		p1++;
//	}
//	char* p2 = p1;
//	if (*p1 >= '0' && *p1 <= '9' || *p1 == '-' || *p1 == '+') {
//		int ans = 0;
//		int temp = 0;
//		if (*p2 == '-') {
//			p1 = p1 + 1;
//			p2 = p2 + 1;
//			while (*p1 >= '0' && *p1 <= '9') {
//				temp++;
//				p1++;
//			}
//			for (int i = temp - 1; i >= 0; --i) {
//				ans += (*p2 - '0') * pow(10, i);
//				p2++;
//			}
//			ans = -ans;
//		}
//		else {
//			while (*p1 >= '0' && *p1 <= '9') {
//				temp++;
//				p1++;
//			}
//			for (int i = temp - 1; i >= 0; --i) {
//				ans += (*p2 - '0') * pow(10, i);
//				p2++;
//			}
//		}
//		return ans;
//	}
//
//	return 0;
//}
// 
int my_atoi(char* str) {
	char* p1;
	p1 = str;
	int flag = 1;
	while (*p1 == ' ') {
		p1++;
	}
	if (*p1 == '-' || *p1 == '+') {
		if (*p1 == '-') {
			flag = -1;
		}
		p1++;
	}
	char* p2 = p1;
	int ans = 0;
	int temp = 0;
	if (*p1 >= '0' && *p1 <= '9') {
		while (*p1 >= '0' && *p1 <= '9') {
			temp++;
			p1++;
		}
		for (int i = temp - 1; i >= 0; --i) {
			ans += (*p2 - '0') * pow(10, i);
			p2++;
		}
		return ans * flag;
	}
	return 0;
}
int main()
{
	char str[20] = "-12dlsjoppwi";
	int val1 = atoi(str);
	int val = my_atoi(str);
	printf("val1 = %d\n", val1);
	printf("val = %d\n", val);
	return 0;
}

