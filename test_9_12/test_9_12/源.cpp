#define _CRT_SECURE_NO_WARNINGS

//荷兰国旗问题
#include <stdio.h>
#include <stdlib.h>
//void swap(int arr[], int a, int b)
//{
//	int temp = arr[a];
//	arr[a] = arr[b];
//	arr[b] = temp;
//}
//
//void nether(int arr[], int left, int right, int p)
//{
//	int less = left - 1;
//	int mere = right + 1;
//	int index = left;
//	while (index < mere)
//	{
//		if (arr[index] < p)
//		{
//			swap(arr, ++less, index++);
//		}
//		else if (arr[index] > p)
//		{
//			swap(arr, --mere, index);
//		}
//		else
//		{
//			index++;
//		}
//	}
//}
//
//int main()
//{
//	int arr[100010];
//	int n = 0;
//	scanf("%d", &n);
//	for (int i = 0; i < n; ++i)
//	{
//		scanf("%d ", arr + i);
//	}
//	int left = 0;
//	int right = n - 1;
//	int p = 0;
//	scanf("%d", &p);
//	nether(arr, left, right, p);
//	for (int i = 0; i < n; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//	return 0;
//}

int cmp(const void* p1, const void* p2)
{
	return ((*(int*)p1) - (*(int*)p2));
}
//
//int main()
//{
//	int arr[] = { 2,3,0,9,8,4,5 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	qsort(arr, sz, sizeof(arr[0]), cmp);
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}

#include<stdio.h>
#include<stdlib.h>
//int compare(const void* e1, const void* e2)
//{
//	int* p1 = (int*)e1;
//	int* p2 = (int*)e2;
//	if (*p1 > *p2)
//	{
//		return 1;
//	}
//	else if (*p1 == *p2)
//	{
//		return 0;
//	}
//	else if (*p1 < *p2)
//	{
//		return -1;
//	}
//}
//int main()
//{
//	int arr[10] = { 1,3,5,7,9,2,4,6,8,10 };
//	qsort(arr, 10, 4, compare);
//	for (int i = 0; i <= 9; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//}


//模拟实现一个qsort函数，默认为升序排序
void swap(char* p1, char* p2, size_t num)
{
	for (int i = 0; i < num; i++)
	{
		char temp = *p1;
		*p1 = *p2;
		*p2 = temp;
		p1++;
		p2++;
	}
}

void bubble_sort(void* base, size_t num, size_t size, int(*cmp)(const void* p1, const void* p2))
{
	for (int i = 0; i < num - 1; ++i)
	{
		for (int j = 0; j < num - 1 - i; ++j)
		{
			if (cmp((char*)base + j * size, (char*)base + (j + 1) * size) > 0)
			{
				swap((char*)base + j * size, (char*)base + (j + 1) * size, size);
			}
		}
	}
}

int cmp_int(const void* p1, const void* p2)
{
	return (*(int*)p1 - *(int*)p2);
}

int main()
{
	int arr[] = { 9,8,7,6,5,4,4,3,2,1,0 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	bubble_sort(arr, sz, sizeof(arr[0]), cmp_int);
	for (int i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	return 0;
}

//size_t