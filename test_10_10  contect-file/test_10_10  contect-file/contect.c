#define _CRT_SECURE_NO_WARNINGS
#include "contect.h"
int cmp_name(const void* p1, const void* p2)
{
	return strcmp(((struct Peoinfor*)p1)->name, ((struct Peoinfor*)p2)->name);
}

//动态初始化通讯录
void Initcontect(contect* con) {
	assert(con);
	con->sz = 0;
	con->capacity = MAX_CAP;
	con->data = calloc(con->capacity, sizeof(Peoinfor));
	if (con->data == NULL) {
		perror("calloc");
		return;
	}
}

void cheak(contect* con) {
	if (con->sz >= con->capacity)
	{
		Peoinfor* pf = (Peoinfor*)realloc(con->data, sizeof(Peoinfor) * (con->capacity + MAX_CAP));
		if (pf != NULL) {
			con->capacity += MAX_CAP;
			con->data = pf;
			printf("扩容成功！\n");
		}
		else
		{
			perror("realloc");
			return;
		}
	}
}

int searchpeo(contect* con, char name[])
{
	assert(con);
	for (int i = 0; i < con->sz; i++)
	{
		if (strcmp(con->data[i].name, name) == 0)
		{
			return i;
		}
	}
	return -1;
}

void DestoryContect(contect* con) {
	free(con->data);
	con->data = NULL;
	con->sz = 0;
	con->capacity = 0;
}

//动态通讯录版本
void Addcontect(contect* con)
{
	assert(con);
	cheak(&con);
	printf("请添加联系人的信息:\n");
	printf("联系人的名字:>");
	scanf("%s", con->data[con->sz].name);
	printf("联系人的年龄:>");
	scanf("%d", &(con->data[con->sz].age));
	printf("联系人的性别:>");
	scanf("%s", con->data[con->sz].sex);
	printf("联系人的电话:>");
	scanf("%s", con->data[con->sz].telephone);
	printf("联系人的地址:>");
	scanf("%s", con->data[con->sz].addr);
	con->sz++;
	printf("增加成功！\n");
}

void Deccontect(contect* con)
{
	assert(con);
	if (con->sz == 0)
	{
		printf("通讯录为空，无需删除!\n");
		return;
	}
	char name[20];
	printf("请输入你要删除联系人的名字:>");
	scanf("%s", name);
	int size = searchpeo(con, name);
	if (size == con->sz - 1)
	{
		con->sz--;
	}
	if (size == -1)
	{
		printf("你要删除的联系人不存在！\n");
		return;
	}
	for (int i = size; i < con->sz - 1; i++)
	{
		con->data[i] = con->data[i + 1];
	}
	con->sz--;
	printf("删除成功！\n");
}

void Seccontect(contect* con)
{
	assert(con);
	if (con->sz == 0)
	{
		printf("通讯录为空，无需查找！\n");
		return;
	}
	char name[20];
	printf("请输入你要查找联系人的名字:>");
	scanf("%s", name);
	int size = searchpeo(con, name);
	if (size == -1)
	{
		printf("你要查找的联系人的信息不存在!\n");
		return;
	}
	printf("%-20s%-5s%-5s%-20s%-30s\n", "名字", "年龄", "性别", "电话", "地址");
	int i = size;
	printf("%-20s%-5d%-5s%-20s%-30s\n",
		con->data[i].name, con->data[i].age, con->data[i].sex,
		con->data[i].telephone, con->data[i].addr);
}

void Meycontect(contect* con) //修改联系人的信息
{
	assert(con);
	if (con->sz == 0)
	{
		printf("通讯录为空，无需查找！\n");
		return;
	}
	char name[20];
	printf("请输入你要修改联系人的名字:>");
	scanf("%s", name);
	int size = searchpeo(con, name);
	if (size == -1)
	{
		printf("你要查找的联系人的信息不存在!\n");
		return;
	}
	printf("请修改联系人的信息:\n");
	printf("联系人的名字:>");
	scanf("%s", con->data[size].name);
	printf("联系人的年龄:>");
	scanf("%d", &(con->data[size].age));
	printf("联系人的性别:>");
	scanf("%s", con->data[size].sex);
	printf("联系人的电话:>");
	scanf("%s", con->data[size].telephone);
	printf("联系人的地址:>");
	scanf("%s", con->data[size].addr);
}

void Showcontect(const contect* con)
{
	assert(con);
	printf("%-20s%-5s%-5s%-20s%-30s\n", "名字", "年龄", "性别", "电话", "地址");
	for (int i = 0; i < con->sz; i++)
	{
		printf("%-20s%-5d%-5s%-20s%-30s\n",
			con->data[i].name, con->data[i].age, con->data[i].sex,
			con->data[i].telephone, con->data[i].addr);
	}
}

void Sortbyname(contect* con)
{
	qsort(&(con->data), con->sz, sizeof(con->data[0]), cmp_name);
	printf("排好序的结构如下:\n");
	printf("%-20s%-5s%-5s%-20s%-30s\n", "名字", "年龄", "性别", "电话", "地址");
	for (int i = 0; i < con->sz; i++)
	{
		printf("%-20s%-5d%-5s%-20s%-30s\n",
			con->data[i].name, con->data[i].age, con->data[i].sex,
			con->data[i].telephone, con->data[i].addr);
	}
}

void Fliecontect(contect* con) {
	assert(con);
	FILE* pf = fopen("contect.txt", "wb");
	if (pf == NULL) {
		perror("Fliecontect");
		return;
	}
	for (int i = 0; i < con->sz; ++i) {
		fwrite(con->data + i, sizeof(Peoinfor), 1, pf);
	}
	fclose(pf);
	pf = NULL;
}

void Readcontect(contect* con) {
	assert(con);
	FILE* pf = fopen("contect.txt", "rb");
	if (pf == NULL) {
		perror("Readcontect");
		return;
	}
	Peoinfor tmp = { 0 };
	while (fread(&tmp, sizeof(Peoinfor), 1, pf)) {
		if (con->sz >= con->capacity)
		{
			printf("通讯录已满，需要扩容！\n");

			Peoinfor* pf1 = (Peoinfor*)realloc(con->data, sizeof(con->data) * (con->capacity + MAX_CAP));
			if (pf1 != NULL) {
				con->capacity += MAX_CAP;
				con->data = pf1;
				printf("扩容成功！\n");
			}
			else
			{
				perror("realloc");
				return;
			}
		}
		con->data[con->sz] = tmp;
		con->sz++;
	}
	fclose(pf);
	pf = NULL;
}