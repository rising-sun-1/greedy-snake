﻿#include <stdio.h>
#include <easyx.h>

//void outtextxy(int x, int y, LPCTSTR str)



int main()
{
	initgraph(800, 600);
	setbkcolor(WHITE);
	cleardevice();

	setbkcolor(RGB(164, 225, 202)); //设置背景颜色为绿色(字体背景)
	settextcolor(BLACK);     //设置字体的颜色，这里设置成黑色
	setlinecolor(BLACK); 
	//setlinestyle(PS_DASH, 5);

	const char* pText = "Helloworld你好，世界";
	outtextxy(0, 0, pText);
	setbkmode(TRANSPARENT);

	//settextstyle(50, 0, "微软雅黑");
	settextstyle(90, 0, "微软雅黑");
	getchar();
	closegraph();
	return 0;
}

