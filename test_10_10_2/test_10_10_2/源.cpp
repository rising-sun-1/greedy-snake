#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <string.h>
#define MAX_NUM 10
//顺序栈的代码
struct stack {
	int data[MAX_NUM];
	int top; //指针
};
//初始化栈的内容
void Initstrack(struct stack s) {
	memset(s.data, 0, sizeof(s.data));
	s.top = -1;
}
//判断栈是否为空
bool NULLstack(struct stack s) {
	if (s.top == -1)
		return true;
	return false;
}

//增加栈中的内容
 bool Addstack(struct stack s, int x) {
	if (s.top == MAX_NUM - 1) {
		return false;
	}
	else {
		s.top += 1;
		s.data[s.top] = x;
		return true;
	}
}
 //减少栈中的内容
 bool Decstack(struct stack s, int x) {
	 if (s.top == -1) {
		 return false;
	 }
	 else {
		 x = s.data[s.top];
		 s.top -= 1;
		 return true;
	 }
}
 //查找栈中的内容
 int Secstack(struct stack s, int x) {
	 if (s.top == -1) {
		 return;
	 }
	 else {
		 int ans = s.data[x - 1];
		 return ans;
	 }
}
//改变栈中的内容
 bool Chastack(struct stack s, int x, int y) {
	 if (s.top == -1) {
		 return false;
	 }
	 else {
		 s.data[x] = y;
		 return true;
	 }
}

//顺序队列的内容
 struct SqQueue {
	 int data[MAX_NUM];
	 int top, down;
 };

 //初始化队列的内容
 void InitSqQueue(struct SqQueue q) {
	 memset(q.data, 0, sizeof(*q.data) * MAX_NUM);
	 q.top = q.down = 0;
 }

 //判断队列是否为空
 bool NULLSqQueue(struct SqQueue q) {
	 if (q.top == q.down) {
		 return true;
	 }
	 else {
		 return false;
	 }
}

//增加队列的内容
 bool AddSqQueue(struct SqQueue q, int x) {
	 if ((q.top + 1) % MAX_NUM == q.down)
		 return false;
	 else {
		 q.data[q.top] = x;
		 q.top = (q.top + 1) % MAX_NUM;
		 return true;
	 }
}

 //减少队列的内容
 bool DecSqQueue(struct SqQueue q, int x) {
	 if (q.top == q.down)
		 return false;
	 else {
		 x = q.data[q.down];
		 q.down = (q.down + 1) % MAX_NUM;
		 return true;
	 }
 }

 //判断队列为空的模板
 struct SqQueue1 {
	 int data[MAX_NUM];
	 int top;
	 int down;
	 int tag; //用size计数
 };

 //

int main(){

	return 0;
}