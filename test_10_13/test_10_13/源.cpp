#define _CRT_SECURE_NO_WARNINGS


//异或和之和
//#include <stdio.h>
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int cnt[21];
//	long long arr[10010];
//	long long ans = 0;
//	for (int i = 0; i < n; i++) {
//		scanf("%lld", arr + i);
//	}
//	int s[10010];
//	s[0] = arr[0];
//	int sum = arr[0];  //赋值为数组的第一项；不漏项
//	for (int i = 1; i < n; i++) {
//		s[i] = s[i - 1] ^ arr[i];
//		sum += s[i]; //从1开始加  范围是0-i
//	}
//
//	//时间复杂度太大了On2
//	for (int i = 0; i < n - 1; i++) {
//		for (int j = i + 1; j < n; j++) {
//			sum += s[j] ^ s[i];
//		}
//	}
//	return 0;
//}

#include <stdio.h>
int main()
{
	int n = 0;
	scanf("%d", &n);
	long long arr[10010];
	for (int i = 1; i <= n; i++) {
		scanf("%d", arr + i);
		arr[i] ^= arr[i - 1];  //异或和前缀和
	}
	long long ans = 0;
	int con[2];  //记录0和1的个数

	for (int j = 0; j < 31; j++) {
		con[0] = con[1] = 0;
		for (int i = 0; i <= n; i++) { 
			con[(arr[i] >> j) & 1]++;
		}
		ans += (1 << j) * con[0] * con[1];
	}
	printf("%d", ans);
	return 0;
}