#define _CRT_SECURE_NO_WARNINGS

#include <bits/stdc++.h>
using namespace std;

const int N = 2000, P = 131;
typedef unsigned long long ull;
int n;
char str[N];
ull h[N], p[N];
vector<ull> vc;

ull get(int l, int r) {
	return h[r] - h[l - 1];
}

int main()
{
	scanf("%d", &n);
	while (n--) {
		scanf("%s", str + 1);
		int len = strlen(str + 1);
		p[0] = 1;
		for (int i = 1; i <= len; i++) {
			p[i] = p[i - 1] * P;
			h[i] = h[i - 1] * P + str[i];
		}

		vc.push_back(get(len + 1, 1));
	}
	sort(vc.begin(), vc.end());
	vc.erase(unique(vc.begin(), vc.end()), vc.end());
	printf("%d\n", vc.size());

	return 0;
}
