#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
void swap(char* a, char* b, size_t size)
{
	for (int i = 0; i < size; ++i)
	{
		char temp = *a;
		*a = *b;
		*b = temp;
		a++;
		b++;
	}
}

//int* partion(int arr[], int left, int right);
//void quicksort(int arr[], int left, int right)
//{
//	if (left < right)
//	{
//		//先随机选一个元素与数组中最后一个元素进行交换，然后进行partion
//		int num = rand() % (right - left + 1); //产生0~sz的任意数字
//		swap(arr, left + num, right);
//		int* ans = (int*)malloc(sizeof(int) * 2);
//		ans = partion(arr, left, right);
//		quicksort(arr, left, ans[0] - 1);
//		quicksort(arr, ans[1] + 1, right);
//	}
//}
//
//int* partion(int arr[], int left, int right)
//{
//	int less = left - 1;
//	int more = right + 1;
//	int index = left;
//	int* ret = (int*)malloc(sizeof(int) * 2);
//	while (index < more)
//	{
//		if (arr[index] < arr[right])
//		{
//			swap(arr, ++less, index++);
//		}
//		else if (arr[index] > arr[right])
//		{
//			swap(arr, --more, index);
//		}
//		else
//			index++;
//	}
//	ret[0] = less + 1;
//	ret[1] = more - 1;
//	return ret;
//}
//
//int main()
//{
//	//模拟实现一个qsort函数
//	int arr[] = { 2,4,5,60,2,1,4 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	int left = 0;
//	int right = sz - 1;
//	quicksort(arr, left, right);
//	for (int i = 0; i < sz; ++i)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//	return 0;
//}

//void bubble_sort(int arr[], int sz)
//{
//	for (int i = 0; i < sz - 1; ++i)
//	{
//		for (int j = 0; j < sz - 1 - i; ++j)
//		{
//			if (arr[j] > arr[j + 1])
//			{
//				swap(arr, j, j + 1);
//			}
//		}
//	}
//}
//
//int main()
//{
//	
//	return 0;
//}

//int cmp_int(const void* p1, const void* p2)
//{
//	return *(int*)p1 - *(int*)p2;
//}
//
//void bubble_sort(void* base, size_t num, size_t size, int (*cmp)(const void*, const void*))
//{
//	for (int i = 0; i < num - 1; ++i)
//	{
//		for (int j = 0; j < num - 1 - i; ++j)
//		{
//			if (cmp((char* )base + j * size, (char*)base + (j + 1) * size) > 0)
//			{
//				swap((char*)base + j * size, (char*)base + (j + 1) * size, size);
//			}
//		}
//	}
//}


//输入整型数组和排序标识，对其元素按照升序或降序进行排序

#include <stdio.h>
#include <stdlib.h>
int cmp_int(const void* p1, const void* p2)
{
    return *(int*)p1 - *(int*)p2;
}
int fcmp_int(const void* p1, const void* p2)
{
    return *(int*)p2 - *(int*)p1;
}
int main()
{
    int n = 0;
    int k = 0;
    int arr[100010];
    scanf("%d", &n);
    for (int i = 0; i < n; ++i)
    {
        scanf("%d ", &arr[i]);
    }
    scanf("%d", &k);
    if (0 == k)
    {
        qsort(arr, n, sizeof(arr[0]), cmp_int);
    }
    if (1 == k)
    {
        qsort(arr, n, sizeof(arr[0]), fcmp_int);
    }
    for (int i = 0; i < n; ++i)
    {
        printf("%d ", arr[i]);
    }
    return 0;
}