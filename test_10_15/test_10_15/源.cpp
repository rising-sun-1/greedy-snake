#define _CRT_SECURE_NO_WARNINGS

 #include <iostream>
 #include <cstring>
 #include <algorithm>

//#include <bits/stdc++.h>
using namespace std;
//typedef long long ll;
//
//const int N = 100010;
//int n = 0;
//ll arr[N];
//
////选择数怎么写
//
//ll c(int a, int b) {
//    ll ret = 1;
//    for (int i = a, j = 1; j <= b; j++, i--) {
//        ret = ret * i / j;
//    }
//    return ret;
//}
//
//int main()
//{
//    scanf("%d", &n);
//    for (int i = 0; i < n; i++) {
//        scanf("%lld", arr + i);
//    }
//    sort(arr, arr + n);
//    int x = 1;
//    while (x < n && arr[x] == arr[0]) x++;
//    int y = x + 1;
//    while (y < n && arr[y] == arr[x]) y++;
//    int z = y + 1;
//    while (z < n && arr[z] == arr[y]) z++;
//    z -= y;
//    y -= x;
//
//    ll ret = 0;
//    if (x >= 3) ret = c(x, 3);
//    else if (x + y >= 3) ret = c(y, 3 - x);
//    else ret = c(z, 1);
//
//    cout << ret << endl;
//}
//
//struct Stu {
//	学生的相关属性
//	char name[20];
//	int age;
//	char sex[5];
//	char add[30];
//};
//
//struct B {
//	int c;
//	char ch;
//};
//
//struct s {
//	char c;
//	int num;
//	int arr[10];
//	double* pd;
//	struct B sb;
//	struct B* ptr;
//};
//
//int main() {
//	double b = 3.78;
//	//按照顺序初始化
//	struct s s1 = { 'a', 3, { 1,2,3 }, &b, {4, 'f'}, NULL }; //局部变量
//	//指定成员初始化
//	struct s s2 = { .num = 100 };
//}
//
//#include <string.h>
//struct S {
//	int age;
//	char name[20];
//};
//
//
//void my_set(struct S t) {
//	t.age = 19;
//	strcpy(t.name, "zhangsan");
//}
//
//void my_set(struct S* ps) {
//	ps->age = 19;
//	strcpy(ps->name, "zhangsan");
//}
//
//void my_print1(struct S t) {
//	printf("%d %s", t.age, t.name);
//}
//
//void my_print2(struct S* ps) {
//	printf("%d %s", ps->age, ps->name);
//}
//
//int main()
//{
//	struct S s = { 0 };
//	my_set(&s);
//}


//这个数组的最长连续子序列
//321 321 321
//3 1 4 1 5 9  3 1 4 1 5 9  3 1 4 1 5 9
 
//75632 75632 75632 75632 ---23567
//一定能够找到最长的子序列

//变相的让我们找不同的个数
const int N = 100010;
int T = 0;
int n = 0;
int arr1[N];
int arr2[N];
int main()
{
	scanf("%d", &T);
	for (int i = 0; i < T; i++) {
		scanf("%d", &n);
		for (int i = 0; i < n; i++) {
			scanf("%d", arr1 + i);
			arr2[arr1[i]]++;
		}
		int count = 0;
		for (int i = 0; i < N; i++) {
			if (arr2[i])
				count++;
		}
		cout << count << endl;
	}

	return 0;
}