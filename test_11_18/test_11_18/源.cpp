#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <cstring>
#include <algorithm>
#include <queue>
using namespace std;

const int N = 310;
typedef pair<int, int> PII;
int m;
int dist[N][N];
int fire[N][N];
queue<PII> q;

int dx[4] = { 0,1,0,-1 }, dy[4] = { 1,0,-1,0 };

int bfs()
{
	q.push({ 0, 0 });
	dist[0][0] = 0;

	while (q.size())
	{
		PII t = q.front();
		q.pop();

		for (int i = 0; i < 4; i++)
		{
			int nx = dx[i] + t.first, ny = dy[i] + t.second;
			if (nx < 0 || ny < 0) continue;
			if (dist[nx][ny])  continue;
			if (fire[nx][ny] <= dist[t.first][t.second] + 1) continue;

			dist[nx][ny] = dist[t.first][t.second] + 1;
			q.push({ nx, ny });
			if (fire[nx][ny] == 0x3f3f3f3f)  return dist[nx][ny];
		}
	}
	return -1;
}

int main()
{
	scanf("%d", &m);
	while (m--)
	{
		int x = 0, y = 0, t = 0;
		scanf("%d%d%d", &x, &y, &t);
		//q.push({x, y});
		//memset(fire, 0x3f, sizeof(dist));
		fire[x][y] = t;
		//��dist����Ԥ���� 
		for (int i = 0; i < 4; i++)
		{
			int nx = dx[i] + x, ny = dy[i] + y;
			if (nx < 0 || nx > 301 || ny < 0 || ny > 301) continue;
			fire[nx][ny] = min(t, fire[nx][ny]);
		}
	}

	int res = bfs();

	printf("%d\n", res);

	return 0;
}