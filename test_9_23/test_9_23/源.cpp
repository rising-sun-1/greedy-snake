#define _CRT_SECURE_NO_WARNINGS

//让 N 只候选猴子围成一圈（最多100只猴子），
//从某位置起顺序编号为 1 ~N 号；
//
//从第 1 号开始报数，每轮从 1 报到 3 ，凡报到 3 的猴子即退出圈子，
//接着又从紧邻的下一只猴子开始同样的报数；
//
//如此不断循环，最后剩下的一只猴子就选为猴王。

#include <stdio.h>
int main()
{
	int n = 0;
	scanf("%d", &n);
	int mankeyking[10000];
	for (int i = 0; i < n; i++)
	{
		mankeyking[i] = 0;
	}
	int sign = 0;
	int remain = 0;
	remain = n;
	int count = 0;
	while (remain > 1)
	{
		if (mankeyking[sign] == 0 && count % 3 == 0)
		{
			mankeyking[sign] = 1;
			count = 1;
			remain = remain - 1;
		}
		else if (mankeyking[sign] == 0)
			count++;
		sign++;
		if (sign == n)
			sign = 0;
	}
	for (int i = 0; i < n; i++)
	{
		if (mankeyking[i] == 0)
		{
			printf("%d\n", i + 1);
		}
	}
	return 0;
}