#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stddef.h>

//struct S {
//	char a;
//	int c;
//	char ch;
//};
//
//
////模拟实现offsetof宏
//#define OFFSETOF(type, nem)  (size_t)&(((type*)0)->nem)
//
//int main()
//{
//	printf("%d\n", offsetof(struct S, a));
//	printf("%d\n", offsetof(struct S, c));
//	printf("%d\n", offsetof(struct S, ch));
//	printf("%d\n", OFFSETOF(struct S, a));
//	printf("%d\n", OFFSETOF(struct S, c));
//	printf("%d\n", OFFSETOF(struct S, ch));
//	return 0;
//}
//


//一道编程题：将一个数字的二进制的奇数位和偶数位进行对调
//，将这个数打印出来

#define SET_OR(num) (((num & 0xaaaaaaaa)>>1)+((num & 0x55555555)<<1))

//1010 1010偶数位
//a    a
//0101 0101奇数位
////5    5
//
//1011
//   0001
//     1010
//
int main()
{
	int num = 0;
	scanf("%d", &num);
	int ans = SET_OR(num);
	printf("%d\n", ans);
	return 0;
}

//#include <string.h>
//#include <stdio.h>
//#include <stddef.h>
//
//int main()
//{
//	int arr[1000];
//	int T = 0;
//	scanf("%d", &T);
//	for (int i = 0; i < T; i++) {
//		int n = 0;
//		scanf("%d", &n);
//		memset(arr, 0, sizeof(arr));
//		for (int j = 0; j < n; j++) {
//			int x = 0;
//			scanf("%d", &x);
//			arr[x]++;
//		}
//		int count = 0;
//		for (int k = 0; k < 1000; k++) {
//			if (arr[k])
//				count++;
//		}
//		printf("%d\n", count);
//	}
//	
//	return 0;
//}

//n   words   groups


