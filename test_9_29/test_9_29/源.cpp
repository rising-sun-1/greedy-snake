#define _CRT_SECURE_NO_WARNINGS

//一个数组中只有两个数字是出现一次，其他所有数字都出现了两次。
//编写一个函数找出这两个只出现一次的数字。
//例如：
//有数组的元素是：1，2，3，4，5，1，2，3，4，6
//只有5和6只出现1次，要找出5和6.

//#include <stdio.h>
//#include <string.h>
//////第一种方法
////int main()
////{
////	int arr1[10000];
////	int arr2[10000];
////	int n = 0; 
////	scanf("%d", &n);
////	for (int i = 0; i < n; i++)
////	{
////		scanf("%d", arr1 + i);
////	}
////	memset(arr2, 0, sizeof(arr2));
////	for (int i = 0; i < n; i++)
////	{
////		arr2[arr1[i]]++;
////	}
////	for (int i = 0; i < n; i++)
////	{
////		if (arr2[i] == 1)
////		{
////			printf("%d ", i);
////		}
////	}
////	printf("\n");
////	return 0;
////}
//
////第二种方法
//int main()
//{
//	int arr[10000];
//	int n = 0;
//	scanf("%d", &n);
//	for (int i = 0; i < n; i++)
//	{
//		scanf("%d", arr + i);
//	}
//	int temp = 0;
//	for (int i = 0; i < n; i++)
//	{
//		temp ^= arr[i];   //temp = 5 ^ 6
//	}
//	int online = temp & (~temp + 1);
//	int ans1 = 0;
//	for (int i = 0; i < n; i++)
//	{
//		if ((online & arr[i]) == 0)
//		{
//			ans1 ^= arr[i];
//		}
//	}
//	int ans2 = temp ^ ans1;
//	if (ans1 > ans2)
//	{
//		int k = ans1;
//		ans1 = ans2;
//		ans2 = k;
//	}
//	printf("%d %d\n", ans1, ans2);
//	return 0;
//}

#include <stdio.h>
#include <assert.h>
#include <string.h>
void* my_strncpy(void* dest, const void* src, size_t num)
{
	assert(dest && src);
	int len = strlen((char*)dest);
	void* ret = dest;
	//如果要拷贝的长度没有超过dest,
	if (num < len)
	{
		while (num--)
		{
			*(char*)dest = *(char*)src;
			dest = (char*)dest + 1;
			src = (char*)src + 1;
		}
	}
	//如果大于
	else
	{
		int temp = len;
		while (len--)
		{
			*(char*)dest = *(char*)src;
			dest = (char*)dest + 1;
			src = (char*)src + 1;
		}
		*((char*)ret + temp) = '\0';
	}
	return ret;
}

void* my_strncat(void* dest, const void* src, size_t num)  //字符串追加函数
{
	assert(dest && src);
	int len = strlen((char*)dest);
	void* ret = dest;
	char* ptr = (char*)dest + len;
	while (num--)
	{
		*(ptr) = *(char*)src;
		ptr = ptr + 1;
		src = (char*)src + 1;
	}
	*ptr = '\0';
	return ret;
}

int main()
{
	char arr1[30] = "xxxxxxxx\0xxxxxx";
	char arr2[] = "djaorji";
	//my_strncpy(arr1, arr2, 4);
	my_strncat(arr1, arr2, 4);
	return 0;
}