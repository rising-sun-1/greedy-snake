#define _CRT_SECURE_NO_WARNINGS


#include <stdio.h>
#include <math.h>

int main()
{
    int arr[1050];
    int n = 0;
    scanf("%d", &n);
    for (int i = 0; i < n; i++) {
        scanf("%d", arr + i);
    }
    int res = 0;
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            int max = i - j + abs(arr[i] - arr[j]);
            if (res < max)
                res = max;
        }
    }
    printf("%d\n", res);
    return 0;
}

