#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//struct stu {
//	int c;
//	double s;
//};
//
//int main()
//{ 
//	int arr1[] = { 1,2,3 };
//	int arr2[] = { 1,2,3 };
//	int arr3[] = { 1,2,3 };
//	int* arr7[3] = { arr1,arr2,arr3 };
//	int(*arr[3])[3] = { &arr1,&arr2,&arr3 };
//	/*for (int i = 0; i < 3; i++) {
//		for (int j = 0; j < 3; j++) {
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}*/
//	for (int i = 0; i < 3; i++) {
//		for (int j = 0; j < 3; j++) {
//			printf("%d ", *(*arr[i] + j));
//		}
//		printf("\n");
//	}
//	for (int i = 0; i < 3; i++) {
//		for (int j = 0; j < 3; j++) {
//			printf("%d ", *(arr7[i] + j));
//		}
//	}
//
//	//模拟实现动态二维数组
//	int** Arr = (int**)malloc(3 * sizeof(int*));
//	for (int i = 0; i < 3; i++) {
//		*(Arr + i) = (int*)malloc(3 * sizeof(int));
//	}
//	for (int i = 0; i < 3; i++) {
//		for (int j = 0; j < 3; j++) {
//			Arr[i][j] = 1;
//		}
//	}
//	for (int i = 0; i < 3; i++) {
//		for (int j = 0; j < 3; j++) {
//			printf("%d ", Arr[i][j]);
//		}
//		printf("\n");
//	}
//	printf("%d", sizeof(struct stu));
//	for (int i = 0; i < 3; i++) {
//		free(Arr[i]);
//	}
//	free(Arr);
//	return 0;

//原来版本
//int my_atoi(char* str) {
//	char* p1;
//	p1 = str;
//	while (*p1 == ' ') {
//		p1++;
//	}
//	char* p2 = p1;
//	if (*p1 >= '0' && *p1 <= '9' || *p1 == '-' || *p1 == '+') {
//		int ans = 0;
//		int temp = 0;
//		if (*p2 == '-') {
//			p1 = p1 + 1;
//			p2 = p2 + 1;
//			while (*p1 >= '0' && *p1 <= '9') {
//				temp++;
//				p1++;
//			}
//			for (int i = temp - 1; i >= 0; --i) {
//				ans += (*p2 - '0') * pow(10, i);
//				p2++;
//			}
//			ans = -ans;
//		}
//		else {
//			while (*p1 >= '0' && *p1 <= '9') {
//				temp++;
//				p1++;
//			}
//			for (int i = temp - 1; i >= 0; --i) {
//				ans += (*p2 - '0') * pow(10, i);
//				p2++;
//			}
//		}
//		return ans;
//	}
//
//
//	return 0;
//}

//}

#include <assert.h>
//将字符串转换为数字
//模拟实现一下atoi库函数
enum Stake {
	Right,
	Mass
}sta;

//改进版本
int my_atoi(char* str) {
	char* p1;
	//表示状态都是错误的
	sta = Mass;
	p1 = str;
	//如果传入空指针
	assert(str);
	//如果传入空字符
	if (*str == '\0') {
		return 0;
	}
    int flag = 1; //标记是正数还是负数
	while (*p1 == ' ') {
		p1++;
	}
	if (*p1 == '-' || *p1 == '+') {
		if (*p1 == '-') {
			flag = -1;
		}
		p1++;
	}
	char* p2 = p1;
	long long ans = 0;
	int temp = 0;
	if (*p1 >= '0' && *p1 <= '9') {
		while (*p1 >= '0' && *p1 <= '9') {
			temp++;
			p1++;
		}
		for (int i = temp - 1; i >= 0; --i) {
			ans += (*p2 - '0') * pow(10, i);
			if (ans > INT_MAX || ans < INT_MIN)
			{
				if (flag == -1)
					return INT_MIN;
				else
					return INT_MAX;
			}
			p2++;
		}
		sta = Right;
		return (int)ans * flag;
	}
	return 0;
}

//int main()
//{
//	char str1[] = "8900000000000000";
//	int ret = my_atoi(str1);
//	if (sta == Right)
//		printf("正确打印出:> %d\n", ret);
//	else
//		printf("错误打印出:> %d\n", ret);
//	return 0;
//}

