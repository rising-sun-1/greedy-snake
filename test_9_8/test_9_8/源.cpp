﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>

//给出一个整型数组 numbers 和一个目标值 target，请在数组中找出两个加起来等于
//目标值的数的下标，返回的下标按升序排列。
//（注：返回的数组下标从1开始算起，保证target一定可以由数组里面2个数字相加得到）

int int_cmp(const void* p1, const void* p2) {
    return (*(int*)p1 - *(int*)p2);
}

int* twoSum(int* numbers, int numbersLen, int target, int* returnSize) {
    // write code here
    int* ret = (int*)malloc(sizeof(int) * 2);
    for (int i = 0; i < numbersLen; i++) {
        for (int j = i + 1; j < numbersLen; j++) {
            if (numbers[i] + numbers[j] == target) {
                ret[0] = i + 1;
                ret[1] = j + 1;
            }
        }
    }

    qsort(ret, 2, sizeof(ret[0]), int_cmp);
    *returnSize = 2;
    return ret;
}

//现在有一个长度为 n 的正整数序列，其中只有一种数值出现了奇数次，
//其他数值均出现偶数次，请你找出那个出现奇数次的数值。

#include <stdio.h>
int main()
{
    int n = 0;
    scanf("%d", &n);
    int arr[2000010];
    int ans = 0;
    for (int i = 0; i < n; i++)
    {
        scanf("%d ", &arr[i]);
    }
    for (int i = 0; i < n; i++)
    {
        ans ^= arr[i];
    }
    printf("%d\n", ans);
    return 0;
    /*  给定一个长度为n的数组nums，请你找到峰值并返回其索引。数组可能包含多个峰值，在这种情况下，返回任何一个所在位置即可。
          1.峰值元素是指其值严格大于左右相邻值的元素。严格大于即不能有等于
          2.假设 nums[-1] = nums[n] =
          −
          ∞
          −∞
          3.对于所有有效的 i 都有 nums[i] != nums[i + 1]
          4.你可以使用O(logN)的时间复杂度实现此问题吗？*/




    int findPeakElement(int* nums, int numsLen) {
        // write code here
        int left = 0;
        int right = numsLen - 1;
        if (nums[0] > nums[1])
            return 0;
        else if (nums[numsLen - 2] < nums[numsLen - 1])
            return numsLen - 1;
        else
        {
            while (left < right)
            {
                int mid = left + ((right - left) >> 1);
                if (nums[mid] < nums[mid + 1])
                {
                    left = mid + 1;
                }
                else
                {
                    right = mid;
                }
            }
        };
        return left;

    }

}

//暴力
int findPeakElement(int* nums, int numsLen) {
    // write code here
    for (int i = 1; i < numsLen; i++)
    {
        if (nums[i - 1] < nums[i] && nums[i] > nums[i + 1])
        {
            return i;
        }
    }
    if (nums[0] > nums[1])    return 0;
    if (nums[numsLen - 2] < nums[numsLen - 1])    return numsLen - 1;
    return -1;
}