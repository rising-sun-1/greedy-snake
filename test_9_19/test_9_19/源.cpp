#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <string.h>
#include <assert.h>
//int main()
//{
//	char arr1[] = "dsgfer";
//	char arr2[] = "dsgfty";
//	printf("%d\n", strcmp(arr1, arr2));
//	return 0;
//}
//
//int main()
//{
//	char arr1[] = "xxxxxxxxxxx";
//	char arr2[] = "asd";
//	printf("%s\n", strncpy(arr1, arr2, 6));
//	return 0;
//}

//模拟实现一下memcpy函数
void* my_memcpy(void* p1, const void* p2, size_t num)
{
	assert(p1 && p2);
	void* ret = p1;
	while (num--)
	{
		*(char*)p1 = *(char*)p2;
		p1 = (char*)p1 + 1;
		p2 = (char*)p2 + 1;
	}
	return ret;
}

//模拟实现一下memmove函数
void* my_memmove(void* dest, const void* src, size_t num)
{
	assert(dest && src);
	void* ret = dest;
	if (dest < src)
	{
		while (num--)
		{
			*(char*)dest = *(char*)src;
			dest = (char*)dest + 1;
			src = (char*)src + 1;
		}
	}
	else
	{
		while (num--)
		{
			*((char*)dest + num) = *((char*)src + num);
		}
	}
	return ret;
}


int main()
{
	int arr1[10] = { 0 };
	int arr2[] = { 2,3,4,5,60,0 };
	my_memcpy(arr1, arr2, 6 * sizeof(int));
	for (int i = 0; i < 6; i++)
	{
		printf("%d ", arr1[i]);
	}
	return 0;
}